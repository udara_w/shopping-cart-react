import { AppBar, Toolbar, Typography } from '@mui/material';
import { grey } from '@mui/material/colors';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import React from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import Products from './Pages/Products';
import Product from './Pages/Product';

const theme = createTheme({
  palette: {
    grey: {
      main: grey[200],
    },
  },
});

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <AppBar position="static" color="grey">
          <Toolbar variant="dense">
            <Typography variant="h6" color="inherit" component="div">
              <Link to={'/'} style={{ textDecoration: 'none', color: 'black' }}>
                <strong>Shoppin Cart</strong>
              </Link>
            </Typography>
          </Toolbar>
        </AppBar>
      </ThemeProvider>

      <Routes>
        <Route path="/product/:id" element={<Product />} />
        <Route path="/" element={<Products />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
