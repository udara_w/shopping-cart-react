import { Favorite, ShoppingCart } from '@mui/icons-material';
import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Container,
  Grid,
  IconButton,
  Typography,
} from '@mui/material';
import axios from 'axios';
import React, { useEffect, useReducer } from 'react';
import { Link } from 'react-router-dom';
import logger from 'use-reducer-logger';
import ProductsSkeleton from './ProductsSkeleton';

const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      return { ...state, loading: false, products: action.payload };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

export default function Products() {
  const [{ loading, error, products }, dispatch] = useReducer(logger(reducer), {
    loading: true,
    error: '',
    products: [],
  });
  // const [products, setProducts] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: 'FETCH_REQUEST' });
      try {
        const result = await axios.get('/api/products');
        dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
      } catch (error) {
        dispatch({ type: 'FETCH_FAIL', payload: error.message });
      }
      // setProducts(result.data);
    };
    fetchData();
  }, []);

  return (
    <Container>
      <h1>Featured products</h1>
      <Grid container>
        {loading ? (
          <ProductsSkeleton />
        ) : error ? (
          <div>{error}</div>
        ) : (
          products.map((product) => (
            <Grid item md={3} key={product.id} sx={{ p: 1 }}>
              <Card sx={{ maxWidth: 345 }}>
                <CardMedia>
                  <Link to={`/product/${product.id}`}>
                    <img width="100%" src={product.image} alt={product.name} />
                  </Link>
                </CardMedia>

                <CardContent>
                  <Typography gutterBottom variant="h6" component="div">
                    <Link
                      to={`/product/${product.id}`}
                      style={{ textDecoration: 'none', color: 'black' }}
                    >
                      {product.name}
                    </Link>
                  </Typography>

                  <Typography gutterBottom variant="body1" component="div">
                    <strong>${product.price}</strong>
                  </Typography>
                </CardContent>
                <CardActions>
                  <IconButton>
                    <Favorite />
                  </IconButton>
                  <IconButton>
                    <ShoppingCart />
                  </IconButton>
                </CardActions>
              </Card>
            </Grid>
          ))
        )}
      </Grid>
    </Container>
  );
}
