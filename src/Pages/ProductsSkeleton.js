import { Grid, Skeleton } from '@mui/material';
import React from 'react';

export default function ProductsSkeleton() {
  return (
    <Grid container>
      {Array.from(new Array(4)).map((item, index) => (
        <Grid item md={3} sx={{ p: 1 }} key={index}>
          <Skeleton
            variant="rectangular"
            animation="wave"
            width={272}
            height={324}
          />
        </Grid>
      ))}
    </Grid>
  );
}
