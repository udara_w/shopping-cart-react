import React from 'react';
import { useParams } from 'react-router-dom';

export default function Product() {
  const params = useParams();
  const { id } = params;
  return <div>{id}</div>;
}
